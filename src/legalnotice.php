<?php

spl_autoload_register(function ($className) {
    static $classMap = [
        /**
         * LegalNotice\\Application\\UI
         */
        'LegalNotice\\Application\\UI\\ILegalNoticeControlFactory' => 'Application/UI/ILegalNoticeControlFactory.php',
        'LegalNotice\\Application\\UI\\LegalNoticeControl' => 'Application/UI/LegalNoticeControl.php',
        /**
         * LegalNotice\\DI
         */
        'LegalNotice\\DI\\LegalNoticeExtension' => 'DI/LegalNoticeExtension.php',
        /**
         * LegalNotice
         */
        'LegalNotice' => 'LegalNotice.php',
    ];
    if (isset($classMap[$className])) {
        require __DIR__ . '/LegalNotice/' . $classMap[$className];
    }
});
