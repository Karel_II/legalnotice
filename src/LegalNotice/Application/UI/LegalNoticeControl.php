<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LegalNotice\Application\UI;

use LegalNotice\LegalNotice;

/**
 * Description of LegalNoticeControl
 *
 * @author Karel
 */
class LegalNoticeControl extends \Nette\Application\UI\Control {

    /**
     *
     * @var string 
     */
    protected $view = 'default';
    /*
     * 
     * @var bool     
     */
    protected $resetTemplateFile = false;

    /**
     * 
     * @param string $functionName __FUNCTION__
     */
    protected function setViewFromFunctionName($functionName = __FUNCTION__) {
        $m = array();
        preg_match('#render(\w+)$#', $functionName, $m);
        if (isset($m[1]) && !empty($m[1])) {
            $view = lcfirst($m[1]);
        } else {
            $view = 'default';
        }

        if (strcmp($view, $this->view) !== 0) {
            $this->view = $view;
            $this->resetTemplateFile = true;
        }
    }

    /**
     * Formats view template file names.
     * @return array
     */
    public function formatTemplateFiles() {
        $dir = dirname($this->getReflection()->getFileName());
        $dir = is_dir("$dir/templates") ? $dir : dirname($dir);
        return array(
            "$dir/templates/$this->view.latte",
            "$dir/templates/$this->view.phtml",
        );
    }

    public function getTemplate() {
        $template = parent::getTemplate();
        if (!$template) {
            return;
        }

        if (!$template->getFile() || $this->resetTemplateFile) { // content template
            $files = $this->formatTemplateFiles();
            foreach ($files as $file) {
                if (is_file($file)) {
                    $template->setFile($file);
                    break;
                }
            }

            if (!$template->getFile()) {
                $file = preg_replace('#^.*([/\\\\].{1,70})\z#U', "\xE2\x80\xA6\$1", reset($files));
                $file = strtr($file, '/', DIRECTORY_SEPARATOR);
                throw new \Exception("Page not found. Missing template '$file'.");
            }
            $this->resetTemplateFile = false;
        }
        return $template;
    }

    /**
     * @var \LegalNotice\LegalNotice
     */
    private $legalNotice = NULL;

    public function __construct(LegalNotice $legalNotice) {
        $this->legalNotice = $legalNotice;
        parent::__construct();
    }

    public function render() {
        $this->template->showLegalNoice = $this->legalNotice->showBanner();
        $this->template->render();
    }

    public function handleAccept() {
        $this->template->showLegalNoice = $this->legalNotice->accept(TRUE)->showBanner();
        $this->redrawControl('legalNoticeAlert');
    }

    public function handleReject() {
        $this->template->showLegalNoice = $this->legalNotice->decline(TRUE)->showBanner();
        $this->redrawControl('legalNoticeAlert');
    }

}
