<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LegalNotice\Application\UI;

/**
 *
 * @author Karel
 */
interface ILegalNoticeControlFactory {

    /**
     * 
     * @return \LegalNotice\Application\UI\LegalNoticeControl
     */
    public function create();
}
