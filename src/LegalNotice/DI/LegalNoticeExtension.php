<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LegalNotice\DI;

/**
 * Description of LegalNoticeExtension
 *
 * @author Karel
 */
class LegalNoticeExtension extends \Nette\DI\CompilerExtension {

    private $defaults = [
        'expiration' => '1 days',
        'autoAccept' => false
    ];

    /**
     * 
     * @return void
     */
    public function loadConfiguration() {
        $this->validateConfig($this->defaults);

        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('legalNotice'))
                ->setFactory(\LegalNotice\LegalNotice::class)
                ->addSetup('setExpiration', [$this->config['expiration']]);

        if ($this->config['autoAccept']) {
            $builder->getDefinition($this->prefix('legalNotice'))
                    ->addSetup('accept');
        }

        $builder->addDefinition($this->prefix('legalNoticeControlFactory'))
                ->setImplement(\LegalNotice\Application\UI\ILegalNoticeControlFactory::class);
    }

}
