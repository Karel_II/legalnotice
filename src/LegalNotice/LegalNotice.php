<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LegalNotice;

use \Nette\Security\Permission,
    \Nette\Security\User;
use \Nette\Utils\Validators;
use \Nette\Http\Request,
    \Nette\Http\Response;

/**
 * Description of LegalNotice
 *
 * @author Karel
 */
class LegalNotice {

    use \Nette\SmartObject;

    const PERMISSION_RESOURCE = 'legalNotice';

    private $expiration = '1 days';

    /**
     *
     * @var \Nette\Security\Permission 
     */
    private $permission = NULL;

    /**
     *
     * @var \Nette\Security\User 
     */
    private $user = NULL;

    /**
     *
     * @var \Nette\Http\Request 
     */
    private $request = NULL;

    /**
     *
     * @var \Nette\Http\Response 
     */
    private $response;

    /**
     *
     * @var bool|null 
     */
    private $accepted = NULL;

    /**
     * 
     * @param Permission $permission
     * @param User $user
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Permission $permission, User $user, Request $request, Response $response) {
        $this->permission = $permission;
        $this->user = $user;
        $this->request = $request;
        $this->response = $response;
        if (!$this->permission->hasResource(self::PERMISSION_RESOURCE)) {
            $this->permission->addResource(self::PERMISSION_RESOURCE);
        }
        $this->checkCookie();
    }

    /**
     * 
     * @return string
     */
    public function getExpiration() {
        return $this->expiration;
    }

    /**
     * 
     * @param string $expiration
     * @return $this
     */
    public function setExpiration($expiration) {
        Validators::assert($expiration, 'string:1..', '$expiration');
        $this->expiration = $expiration;
        return $this;
    }

    /**
     * 
     * @return $this
     */
    private function checkCookie() {
        if ($this->request->getCookie(self::PERMISSION_RESOURCE, FALSE) !== FALSE) {
            $this->accepted = TRUE;
            $this->permission->allow(Permission::ALL, self::PERMISSION_RESOURCE);
        } else {
            $this->accepted = FALSE;
            $this->permission->deny(Permission::ALL, self::PERMISSION_RESOURCE);
        }
        return $this;
    }

    /**
     * 
     * @return boolean
     */
    public function showBanner() {
        return $this->accepted !== TRUE;
    }

    /**
     * 
     * @return boolean
     */
    public function allowContent() {
        return $this->user->isAllowed(self::PERMISSION_RESOURCE);
    }

    /**
     * 
     * @param boolean $save
     * @return $this
     */
    public function accept($save = FALSE) {
        $this->accepted = TRUE;
        $this->permission->allow(Permission::ALL, self::PERMISSION_RESOURCE);
        if ($save === TRUE) {
            $this->response->setCookie(self::PERMISSION_RESOURCE, TRUE, $this->expiration);
        }
        return $this;
    }

    /**
     * 
     * @param boolean $save
     * @return $this
     */
    public function decline($save = FALSE) {
        if ($save === TRUE) {
            $this->response->deleteCookie(self::PERMISSION_RESOURCE);
        }
        $this->accepted = FALSE;
        $this->permission->deny(Permission::ALL, self::PERMISSION_RESOURCE);
        return $this;
    }

}
